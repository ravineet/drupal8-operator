# Drupal 8 operator

Generate project:

`operator-sdk new drupal8-operator --api-version=drupal8.cern/v1alpha1 --kind=Drupal8 --type=ansible`

`operator-sdk new drupal8-operator --cluster-scoped --type ansible --kind Drupal8 --api-version drupal8.cern/v1alpha1`

## Build and run the operator

Original source: [https://github.com/operator-framework/operator-sdk/blob/master/doc/ansible/user-guide.md#build-and-run-the-operator](https://github.com/operator-framework/operator-sdk/blob/master/doc/ansible/user-guide.md#build-and-run-the-operator)


Before running the operator, Kubernetes needs to know about the new custom resource definition the operator will be watching.

```bash
oc create -f deploy/crds/drupal8_v1alpha1_drupal8_crd.yaml
```

//TODO

[https://blog.openshift.com/reaching-for-the-stars-with-ansible-operator/](https://blog.openshift.com/reaching-for-the-stars-with-ansible-operator/)
[https://github.com/djzager/ansible-role-hello-world-k8s/](https://github.com/djzager/ansible-role-hello-world-k8s/)

## Deploy

Only once:
`oc create -f .\deploy\crds\drupal8_v1alpha1_drupal8_crd.yaml`

LOOP:

`oc apply -f .\deploy\`

`oc adm policy add-cluster-role-to-user admin -z drupal8-operator`

`oc apply -f .\deploy\crds\drupal8_v1alpha1_drupal8_cr.yaml`

END:
`oc delete -f .\deploy\`

## Prepare secrets

Prepare `drupal8-operator-credentials.yaml`:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: drupal8-operator-credentials
  namespace: drupal8-operator
type: Opaque
stringData:
  openshift_public_hostname: "xxx"
  db_host_value: "xxx"
  db_port_value: "xxx"
  db_admin_user_value: "xxx"
  db_admin_password_value: "xxx"
  druman_gitlab_user_token: "xxx"
```

and then execute:

`oc create -f drupal8-operator-credentials.yaml`

It will be attached to the `operator.yaml` in:

```yaml
...
envFrom:
  - secretRef:
      name: drupal8-operator-credentials
...
```

## Errors

```bash
E0508 10:28:37.825516       1 reflector.go:134] sigs.k8s.io/controller-runtime/pkg/cache/internal/informers_map.go:196: Failed to list *unstructured.Unstructured: deploymentconfigs.apps.openshift.io is forbidden: User "system:serviceaccount:drupal8-operator:drupal8-operator" cannot list deploymentconfigs.apps.openshift.io at the cluster scope: no RBAC policy matched
```

Consider assign `oc adm policy add-cluster-role-to-user admin -z drupal8-operator` admin [cluster]role to the service account.

Interesting for secrets:
http://willthames.github.io/2019/01/28/immutable-kubernetes-configuration-with-ansible.html

## Namespace according to cluster-scope or namespace-scope

https://github.com/justinbarrick/flux-operator#overview