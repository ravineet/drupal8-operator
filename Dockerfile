FROM quay.io/operator-framework/ansible-operator:v0.7.0

#USER root
# Install requirements for mysql_user and mysql_db ansible modules
# More info: 
#RUN yum install -y python2-PyMySQL \
#   && yum clean all \
#   && rm -rf /var/cache/yum

# Install requirements for using GitLab
# Due to https://github.com/ansible/ansible/issues/44564, install ansible==2.8.0.rc2
#RUN pip install ansible==2.8.0rc2 python-gitlab

# To install a role from ansible galaxy:
#RUN ansible-galaxy install geerlingguy.mysql

# Install oc client
#RUN curl -L https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz | tar xvz -C /bin/ --strip-components=1
#RUN ansible-galaxy install andrewrothstein.openshift-origin-client-tools

#USER 1001

COPY watches.yaml ${HOME}/watches.yaml

COPY ansible/roles/ ${HOME}/roles/
COPY ansible/playbook.yaml ${HOME}/playbook.yaml
